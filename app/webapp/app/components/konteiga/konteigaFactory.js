angular.module('konteigaApp')

// performHttp - allows us to do a POST or GET... may implement other method types at a later stage...
.factory('performHttp', ['$http', function($http) {
    var postToUrl = function(data, url) {
        return $http({method: 'POST', url: url, data: data, transformRequest: angular.identity, headers: {'Content-Type': undefined}}).then(function(result) {
            return result.data;
        });
    };

    var getFromUrl = function(url) {
        return $http({method: 'GET', url: url, transformRequest: angular.identity, headers: {'Content-Type': undefined}}).then(function(result) {
            return result.data;
        })
    };

    var deleteUrl = function(url) {
        return $http({method: 'DELETE', url: url, transformRequest: angular.identity, headers: {'Content-Type': undefined}}).then(function(result) {
            return result.data;
        })
    };

    return {
        postToUrl: postToUrl,
        getFromUrl: getFromUrl,
        deleteUrl: deleteUrl
    };
}]);
