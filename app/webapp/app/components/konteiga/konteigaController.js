angular.module('konteigaApp')

.controller('KonteigaCtrl', ['$scope', '$sce', 'performHttp', '$rootScope', function($scope, $sce, performHttp, $rootScope) {
    // when ace editor is loaded... set the session!
    $scope.aceLoaded = function(_editor) {
        _editor.setOptions({
            minLines: 15,
            maxLines: 15
        });
        $scope.aceSession = _editor.getSession();
    };

    // not sure how much of a performance hit this will cause...
    var allItems = performHttp.getFromUrl('/getAll');
    allItems.then(function(getAllResult) {
        $rootScope.allItems = getAllResult;
    });

    // save button click method
    $scope.saveItem = function() {
        if ($scope.title) {
            var postData = {
                title: $scope.title,
                data: $scope.aceSession.getValue()
            };

            var returnedHtml = performHttp.postToUrl(JSON.stringify(postData), '/n/item');

            returnedHtml.then(function(result) {
                var allItems = performHttp.getFromUrl('/getAll');
                allItems.then(function(getAllResult) {
                    $rootScope.allItems = getAllResult;
                });

                // clear the title and ace editor
                $scope.title = "";
                $scope.aceSession.setValue("");

                $scope.parsedItem = $sce.trustAsHtml(result);
            });
        }
    }
}])

.controller('ViewItemCtrl', ['$rootScope', '$scope', '$sce', 'performHttp', '$routeParams', '$location', function($rootScope, $scope, $sce, performHttp, $routeParams, $location) {
    $scope.parsedItem = $sce.trustAsHtml("<b>Loading...</b><br/><p></p>");

    // not sure how much of a performance hit this will cause...
    var allItems = performHttp.getFromUrl('/getAll');
    allItems.then(function(getAllResult) {
        $rootScope.allItems = getAllResult;
    });

    var data = performHttp.getFromUrl('/v/item?key=' + $routeParams.itemId);
    data.then(function(itemResult) {
        $scope.itemId = $routeParams.itemId;
        $scope.parsedItem = $sce.trustAsHtml(itemResult);
    });

    // edit button click method
    $scope.editItem = function() {
        // we want to edit this item...
        $location.path('/e/item/' + $scope.itemId);
    }

    // delete button click method
    $scope.deleteItem = function() {
        var data = performHttp.deleteUrl('/d/item?key=' + $routeParams.itemId);
        data.then(function(itemResult) {
            var allItems = performHttp.getFromUrl('/getAll');
            allItems.then(function(getAllResult) {
                $rootScope.allItems = getAllResult;
                $location.path('/main');
            });
        });
    }
}])

.controller('EditItemCtrl', ['$rootScope', '$scope', '$sce', 'performHttp', '$routeParams', '$location', function($rootScope, $scope, $sce, performHttp, $routeParams, $location) {
    $scope.parsedItem = $sce.trustAsHtml("<b>Loading...</b><br/><p></p>");

    $scope.aceLoaded = function(_editor) {
        _editor.setOptions({
            minLines: 15,
            maxLines: 15
        });
        _editor.$blockScrolling = Infinity;
        $scope.aceSession = _editor.getSession();
    };

    var data = performHttp.getFromUrl('/s/item?key=' + $routeParams.itemId);
    data.then(function(itemResult) {
        $scope.title = itemResult.Title;
        $scope.aceSession.setValue(itemResult.Data);

        $scope.parsedItem = '';
    });

    $scope.saveItem = function() {
        if ($scope.title) {
            var postData = {
                keyid: parseInt($routeParams.itemId),
                title: $scope.title,
                data: $scope.aceSession.getValue()
            };
            var returnedHtml = performHttp.postToUrl(JSON.stringify(postData), '/u/item');

            returnedHtml.then(function(result) {
                // update the list to reflect any new changes to the title...
                var allItems = performHttp.getFromUrl('/getAll');
                allItems.then(function(getAllResult) {
                    $rootScope.allItems = getAllResult;
                });

                // go back to the "view" page
                $location.path('/item/' + $routeParams.itemId);
            });
        }
    }
}]);
