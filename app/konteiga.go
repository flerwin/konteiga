package konteiga

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/microcosm-cc/bluemonday"
	"github.com/russross/blackfriday"

	"bitbucket.org/flerwin/konteiga/storage"
)

func init() {
	http.HandleFunc("/", defaultHandler)
	http.HandleFunc("/n/item", newItemHandler)
	http.HandleFunc("/v/item", viewItemHandler)
	http.HandleFunc("/u/item", updateItemHandler)
	http.HandleFunc("/s/item", structItemHandler)
	http.HandleFunc("/d/item", deleteItemHandler)
	http.HandleFunc("/getAll", getAllHandler)
}

func defaultHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "konteiga")
}

// inputStruct - this is the structure used when data is posted to us
type inputStruct struct {
	KeyID int64
	Title string
	Data  string
}

func structItemHandler(w http.ResponseWriter, r *http.Request) {
	theKeyString := r.URL.Query()["key"][0]

	theKeyInt, _ := strconv.ParseInt(theKeyString, 10, 64)
	theTitle, theData := storage.GetTitleMarkdownData(r, theKeyInt)

	w.Header().Set("Content-Type", "application/json")

	var theStruct = inputStruct{
		Title: theTitle,
		Data:  string(theData),
	}
	theStructBytes, _ := json.Marshal(theStruct)
	w.Write(theStructBytes)
}

func viewItemHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		theKeyString := r.URL.Query()["key"][0]

		theKeyInt, _ := strconv.ParseInt(theKeyString, 10, 64)
		theData := storage.GetParsedData(r, theKeyInt)

		w.Header().Set("Content-Type", "text/html")
		w.Write(theData)
	} else {
		fmt.Fprintln(w, "Not GET")
	}
}

func newItemHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		// TODO: do something with the error result
		theData, _ := ioutil.ReadAll(r.Body)

		var theDataStruct inputStruct

		json.Unmarshal(theData, &theDataStruct)

		unsafeOutput := blackfriday.MarkdownCommon([]byte(theDataStruct.Data))

		// make sure we don't have any dodgy code!
		safeOutput := bluemonday.UGCPolicy().SanitizeBytes(unsafeOutput)

		storage.NewData(r, theDataStruct.Title, []byte(theDataStruct.Data), safeOutput)

		fmt.Fprintln(w, "Added "+theDataStruct.Title)
	} else {
		fmt.Fprintln(w, "Not POST")
	}
}

func deleteItemHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "DELETE" {
		theKeyString := r.URL.Query()["key"][0]

		theKeyInt, _ := strconv.ParseInt(theKeyString, 10, 64)
		storage.DeleteData(r, theKeyInt)
	} else {
		fmt.Fprintln(w, "Not DELETE")
	}
}

func updateItemHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		// TODO: do something with the error result
		theData, _ := ioutil.ReadAll(r.Body)

		// ad the moment, we dont worry about what is in the data... reason... is because angularjs has taken care of pre-populating all the fields...
		var theDataStruct inputStruct

		json.Unmarshal(theData, &theDataStruct)

		unsafeOutput := blackfriday.MarkdownCommon([]byte(theDataStruct.Data))

		safeOutput := bluemonday.UGCPolicy().SanitizeBytes(unsafeOutput)

		storage.UpdateData(r, theDataStruct.KeyID, theDataStruct.Title, []byte(theDataStruct.Data), safeOutput)

		fmt.Fprintln(w, "Updated "+theDataStruct.Title)
	} else {
		fmt.Fprintln(w, "Not POST")
	}
}

func getAllHandler(w http.ResponseWriter, r *http.Request) {
	theItems := storage.GetDataList(r)

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")

	theData, _ := json.Marshal(theItems)

	w.Write(theData)
}
