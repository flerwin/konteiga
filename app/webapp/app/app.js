'use strict';

// Declare app level module which depends on views, and components
angular.module('konteigaApp', [
  'ngRoute',
  'ngMessages',
  'ui.ace'
])

.run(['$rootScope', 'performHttp', function($rootScope, performHttp) {
    var allItems = performHttp.getFromUrl('/getAll')

    allItems.then(function(result) {
        $rootScope.allItems = result;
    });
}])

.config(function($routeProvider) {
    $routeProvider
        .when('/main', {
            templateUrl: 'templates/main.html',
            controller: 'KonteigaCtrl'
        })
        .when('/item/:itemId', {
            templateUrl: 'templates/item.html',
            controller: 'ViewItemCtrl'
        })
        .when('/e/item/:itemId', {
            templateUrl: 'templates/edititem.html',
            controller: 'EditItemCtrl'
        })
        .otherwise({
            redirectTo: '/main'
        });
});
