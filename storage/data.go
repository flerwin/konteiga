package storage

import (
	"net/http"
	"strconv"

	"golang.org/x/net/context"

	"google.golang.org/appengine"
	"google.golang.org/appengine/datastore"
	"google.golang.org/appengine/memcache"
)

// KonteigaData - Main struct for holding data used within Konteiga
type KonteigaData struct {
	Title        string
	MarkdownData []byte
	ParsedData   []byte
}

// NewData - Stores the data given and returns back the key
func NewData(r *http.Request, title string, markdownData []byte, parsedData []byte) int64 {
	ctx := appengine.NewContext(r)

	// create new structure
	newData := KonteigaData{
		Title:        title,
		MarkdownData: markdownData,
		ParsedData:   parsedData,
	}

	// place the data into the datastore
	newKey, _ := datastore.Put(ctx, datastore.NewIncompleteKey(ctx, "konteigadata", nil), &newData)

	// get the new key created as a string
	newKeyID := newKey.IntID()

	// use this key to create a memcache record with the parsed data
	memcacheItem := &memcache.Item{
		Key:   strconv.FormatInt(newKeyID, 10),
		Value: []byte(parsedData),
	}

	// add the information to memcache for later retrieval
	if err := memcache.Add(ctx, memcacheItem); err == memcache.ErrNotStored {
		// key already exists
	} else if err != nil {
		// some other error has occured!
	}

	return newKeyID
}

// getKonteigaData - Get the data from the datastore
func getKonteigaData(ctx *context.Context, key int64) (KonteigaData, *datastore.Key) {
	// setup a Key for the datastore
	existingKey := datastore.NewKey(*ctx, "konteigadata", "", key, nil)

	// setup the structure
	var existingData KonteigaData

	// TODO: deal with any errors
	datastore.Get(*ctx, existingKey, &existingData)

	return existingData, existingKey
}

// GetTitleMarkdownData - Gets the markdown data using the given key
func GetTitleMarkdownData(r *http.Request, key int64) (string, []byte) {
	// markdown data _always_ comes from the datastore, since we will only be obtaining this to modify...

	// get the context
	ctx := appengine.NewContext(r)

	existingData, _ := getKonteigaData(&ctx, key)

	// return back the markdown data!
	return existingData.Title, existingData.MarkdownData
}

// GetParsedData - Gets the parsed data using the given key
func GetParsedData(r *http.Request, key int64) []byte {
	// parsed data should come from memcache if it can, otherwise get from datastore

	// get the context
	ctx := appengine.NewContext(r)

	// try and obtain the item from memcache
	if item, err := memcache.Get(ctx, strconv.FormatInt(key, 10)); err == memcache.ErrCacheMiss {
		// item is not in memcache... get from datastore!
		existingData, _ := getKonteigaData(&ctx, key)

		if &existingData != nil {
			// add this data to memcache for later use
			memcacheItem := &memcache.Item{
				Key:   strconv.FormatInt(key, 10),
				Value: []byte(existingData.ParsedData),
			}

			// add the information to memcache for later retrieval
			if err := memcache.Add(ctx, memcacheItem); err == memcache.ErrNotStored {
				// key already exists
			} else if err != nil {
				// some other error has occured!
			}
			return []byte(existingData.ParsedData)
		}
	} else if err != nil {
		// there is an error getting the item!
	} else {
		// yay, we have got the value from memcache!
		return item.Value
	}

	return nil
}

// UpdateData - Updates the data using the given key
func UpdateData(r *http.Request, key int64, title string, markdownData []byte, parsedData []byte) {
	// get the context
	ctx := appengine.NewContext(r)

	// get the existing data
	existingData, existingKey := getKonteigaData(&ctx, key)

	if &existingData != nil {
		// update the structure
		existingData.Title = title
		existingData.MarkdownData = markdownData
		existingData.ParsedData = parsedData

		// update the datastore
		datastore.Put(ctx, existingKey, &existingData)

		// update the memcache
		memcacheItem := &memcache.Item{
			Key:   strconv.FormatInt(key, 10),
			Value: []byte(existingData.ParsedData),
		}

		if err := memcache.Set(ctx, memcacheItem); err == memcache.ErrNotStored {
			// key already exists
		} else if err != nil {
			// some other error has occured!
		}
	}
}

// DeleteData - Delete the data using the given key
func DeleteData(r *http.Request, key int64) {
	// get the context
	ctx := appengine.NewContext(r)

	// get the existing key
	existingKey := datastore.NewKey(ctx, "konteigadata", "", key, nil)

	// delete the data from the datastore
	// TODO: Check the error
	datastore.Delete(ctx, existingKey)

	// delete the data from memcache
	// TODO: Check the error
	memcache.Delete(ctx, strconv.FormatInt(key, 10))
}

// DataListItem - Item within the Data List
type DataListItem struct {
	KeyID int64
	Title string
}

// GetDataList - Get a list of data
func GetDataList(r *http.Request) []DataListItem {
	ctx := appengine.NewContext(r)

	q := datastore.NewQuery("konteigadata").Project("Title")

	var theDataList []DataListItem
	theKeys, _ := q.GetAll(ctx, &theDataList)

	// the amount of keys _should_ be the same as the amount of items in theDataList
	for i, currentKey := range theKeys {
		theDataList[i].KeyID = currentKey.IntID()
	}

	return theDataList
}
